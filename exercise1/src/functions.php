<?php

namespace RstExercise;

/**
 * @param array $floatArray
 * @return array
 */
function onlyIfSquareAbove26(array $floatArray = [])
{
    foreach ($floatArray as $key => $item) {
        if (is_numeric($item) && $item ** 2 > 26)
            continue;
        unset($floatArray[$key]);
    }

    return $floatArray;
}