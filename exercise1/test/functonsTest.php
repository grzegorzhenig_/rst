<?php

namespace RstExerciseTest;

require_once '../src/functions.php';

use RstExercise;
use PHPUnit\Framework\TestCase;

class tackTest extends TestCase
{
    /**
     * @param $testArray
     * @param $expected
     * @dataProvider onlyIfSquareAbove26DataProvider
     */
    function testOnlyIfSquareAbove26($testArray, $expected)
    {

        $result = RstExercise\onlyIfSquareAbove26($testArray);

        if (count($result) !== count($expected)) {
            $this->assertTrue(false);
            return;
        }
        foreach ($result as $value) {
            if (in_array($value, $expected))
                continue;

            $this->assertTrue(false);
            return;
        }
        $this->assertTrue(true);
    }

    /**
     * @return array
     */
    public function onlyIfSquareAbove26DataProvider()
    {
        return [
            [[2, 26, 3.4, 15.4, 'key' => 40], [26, 15.4, 40]],
            [[4, 6000, 'string'], [6000]],
        ];
    }
}