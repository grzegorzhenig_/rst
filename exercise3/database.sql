CREATE DATABASE IF NOT EXISTS `rst_exercise3`
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `id`       BIGINT(20) UNSIGNED      AUTO_INCREMENT PRIMARY KEY,
  `name`     VARCHAR(200)    NOT NULL UNIQUE,
  `password` CHAR(60) BINARY NOT NULL,
  `email`    VARCHAR(70)     NOT NULL UNIQUE,
  `joined`   DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active`   BINARY(1)       NOT NULL DEFAULT '0'
);

CREATE TABLE `friends_relations` (
  `id`         BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `user_id`    BIGINT(20) UNSIGNED NOT NULL,
  `friendid`   BIGINT(20) UNSIGNED NOT NULL,
  `expired_on` DATETIME            NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (friendid) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  UNIQUE KEY `UQ_friends` (`user_id`, `friendid`)
);

CREATE TABLE `email_confirmation` (
  `id`         BIGINT(20) UNSIGNED          AUTO_INCREMENT PRIMARY KEY,
  `user_id`    BIGINT(20) UNSIGNED NOT NULL,
  `key`        VARCHAR(60)         NOT NULL DEFAULT '',
  `expired_on` DATETIME            NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `user_browsers` (
  `id`         BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `time`       DATETIME            DEFAULT CURRENT_TIMESTAMP,
  `user_id`    BIGINT(20) UNSIGNED NOT NULL,
  `ipv4`       INT UNSIGNED,
  `ipv6`       BINARY(16),
  `status`     VARCHAR(10),
  `user_agent` VARCHAR(200),
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id`         BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `time`       DATETIME            NOT NULL
                                   DEFAULT CURRENT_TIMESTAMP,
  `user_id`    BIGINT(20) UNSIGNED NOT NULL,
  `ipv4`       INT UNSIGNED,
  `ipv6`       BINARY(16),
  `user_agent` VARCHAR(200),
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `errors_log` (
  `id`          BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `time`        DATETIME            DEFAULT CURRENT_TIMESTAMP,
  `user_id`     BIGINT(20) UNSIGNED NOT NULL,
  `ipv4`        INT UNSIGNED,
  `ipv6`        BINARY(16),
  `user_agent`  VARCHAR(200),
  `type`        VARCHAR(10),
  `description` VARCHAR(400)
);
CREATE TABLE IF NOT EXISTS `images` (
  `id`          BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `path`        VARCHAR(200)        NOT NULL,
  `name`        VARCHAR(100)        NOT NULL,
  `alt`         VARCHAR(100)        NOT NULL,
  `ovner`       BIGINT(20) UNSIGNED NOT NULL,
  `description` LONGTEXT            NOT NULL,
  `mark`        DECIMAL             DEFAULT NULL,
  `no_of_marks` INT                 DEFAULT 0,
  `type`        VARCHAR(20)         NOT NULL,
  FOREIGN KEY (ovner) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS `image_marks` (
  `id`       BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `mark`     DECIMAL             DEFAULT NULL,
  `user_id`  BIGINT(20) UNSIGNED NOT NULL,
  `image_id` BIGINT(20) UNSIGNED NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (image_id) REFERENCES images (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  UNIQUE KEY `UQ_UserId_ImageId` (`user_id`, `image_id`)
);


CREATE TABLE IF NOT EXISTS `tags` (
  `id`  BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `tag` VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `image_tags_relations` (
  `id`       BIGINT(20) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `tag_id`   BIGINT(20) UNSIGNED NOT NULL,
  `image_id` BIGINT(20) UNSIGNED NOT NULL,
  FOREIGN KEY (tag_id) REFERENCES tags (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (image_id) REFERENCES images (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);


DELIMITER //
CREATE TRIGGER `confirmation_insert`
BEFORE INSERT ON `email_confirmation`
FOR EACH ROW
  IF (NEW.expired_on <= CURRENT_TIMESTAMP)
  THEN
    SET NEW.expired_on = DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 5 DAY);
  END IF;
//

CREATE PROCEDURE updateMark(update_id BIGINT(20) UNSIGNED)
  BEGIN
    UPDATE images
      LEFT JOIN (
                  SELECT
                    `image_id`,
                    COUNT(*)    AS nomber,
                    AVG(`mark`) AS average
                  FROM image_marks
                  WHERE image_marks.image_id = update_id
                )
        AS a ON images.id = a.image_id
    SET
      images.mark        = a.average,
      images.no_of_marks = CASE
                           WHEN a.nomber IS NULL
                             THEN 0
                           ELSE a.nomber
                           END
      WHERE images.id = update_id;
  END;
//

CREATE TRIGGER `mark_update_on_insert`
AFTER INSERT ON `image_marks`
FOR EACH ROW
  CALL updateMark(NEW.image_id);
//

CREATE TRIGGER `mark_update_on_update`
AFTER UPDATE ON `image_marks`
FOR EACH ROW
  CALL updateMark(NEW.image_id);
//

CREATE TRIGGER `mark_update_on_delete`
AFTER DELETE ON `image_marks`
FOR EACH ROW
  CALL updateMark(OLD.image_id);
//
DELIMITER ;